import 'package:flutter/material.dart';

TextEditingController modelName = TextEditingController();
TextEditingController compName = TextEditingController();
TextEditingController imgUrl = TextEditingController();
TextEditingController camera = TextEditingController();
TextEditingController battery = TextEditingController();
TextEditingController performance = TextEditingController();
TextEditingController gaming = TextEditingController();
TextEditingController charging = TextEditingController();
TextEditingController display = TextEditingController();
TextEditingController security = TextEditingController();
TextEditingController ui = TextEditingController();
TextEditingController price = TextEditingController();
TextEditingController valueformoney = TextEditingController();
TextEditingController network = TextEditingController();
TextEditingController hardware = TextEditingController();
TextEditingController design = TextEditingController();
