import 'package:basic_apk/main.dart';
// ignore: depend_on_referenced_packages
import 'package:sqflite/sqflite.dart';

class Post {
  bool isLiked;
  int likeCount;
  final String? category;
  final String? imgUrl;
  final String? compName;
  final String? modelName;
  final String? camera;
  final String? battery;
  final String? performance;
  final String? gaming;
  final String? charging;
  final String? display;
  final String? security;
  final String? ui;
  final String? price;
  final String? valueformoney;
  final String? network;
  final String? hardware;
  final String? design;

  Post({
    this.category,
    this.compName,
    this.imgUrl,
    this.modelName,
    this.isLiked = false,
    this.likeCount = 0,
    this.battery,
    this.camera,
    this.charging,
    this.design,
    this.display,
    this.gaming,
    this.hardware,
    this.network,
    this.performance,
    this.price,
    this.security,
    this.ui,
    this.valueformoney,
  });

  Map<String, dynamic> toMap() {
    return {
      'category': category,
      'imgUrl': imgUrl,
      'compName': compName,
      'modelName': modelName,
      'battery': battery,
      'camera': camera,
      'performance': performance,
      'gaming': gaming,
      'charging': charging,
      'display': display,
      'security': security,
      'ui': ui,
      'price': price,
      'valueformoney': valueformoney,
      'network': network,
      'hardware': hardware,
      'design': design,
    };
  }
}

Future insertPost(Post post) async {
  dynamic localDB = await database;

  await localDB.insert(
    "Post",
    post.toMap(),
    conflictAlgorithm: ConflictAlgorithm.ignore,
  );
}

Future<List<Post>> getPosts() async {
  dynamic localDB = await database;

  List<Map<String, dynamic>> list = await localDB.query("Post");

  return List.generate(list.length, (index) {
    return Post(
      category: list[index]['category'],
      compName: list[index]['compName'],
      imgUrl: list[index]['imgUrl'],
      modelName: list[index]['modelName'],
      battery: list[index]['battery'],
      camera: list[index]['camera'],
      charging: list[index]['charging'],
      design: list[index]['design'],
      display: list[index]['display'],
      gaming: list[index]['gaming'],
      hardware: list[index]['hardware'],
      network: list[index]['network'],
      performance: list[index]['performance'],
      price: list[index]['price'],
      security: list[index]['security'],
      ui: list[index]['ui'],
      valueformoney: list[index]['valueformoney'],
    );
  });
}

List posts = [];

Future addToPosts() async {
  List<Post> list = await getPosts();

  for (Post p in list) {
    posts.add(p);
  }
}
