import 'package:flutter/material.dart';
import 'package:basic_apk/pages/posts.dart';

String likeCountDisplay(int likes) {
  if (likes == 0) {
    return "";
  } else if (likes == 1) {
    return "$likes like";
  } else {
    return "$likes likes";
  }
}

Column getInfo(int index) {
  return Column(
    children: [
      const SizedBox(
        height: 20,
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          const Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text(
                "Category : ",
                style: TextStyle(
                  color: Color.fromARGB(255, 200, 173, 246),
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Text(
                "Company Name : ",
                style: TextStyle(
                  color: Color.fromARGB(255, 200, 173, 246),
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Text(
                "Model Name : ",
                style: TextStyle(
                  color: Color.fromARGB(255, 200, 173, 246),
                ),
              ),
              Text(
                "Battrey : ",
                style: TextStyle(
                  color: Color.fromARGB(255, 200, 173, 246),
                ),
              ),
              Text(
                "Camera: ",
                style: TextStyle(
                  color: Color.fromARGB(255, 200, 173, 246),
                ),
              ),
              Text(
                "Performance: ",
                style: TextStyle(
                  color: Color.fromARGB(255, 200, 173, 246),
                ),
              ),
              Text(
                "Gaming: ",
                style: TextStyle(
                  color: Color.fromARGB(255, 200, 173, 246),
                ),
              ),
              Text(
                "Charging: ",
                style: TextStyle(
                  color: Color.fromARGB(255, 200, 173, 246),
                ),
              ),
              Text(
                "Display : ",
                style: TextStyle(
                  color: Color.fromARGB(255, 200, 173, 246),
                ),
              ),
              Text(
                "Security: ",
                style: TextStyle(
                  color: Color.fromARGB(255, 200, 173, 246),
                ),
              ),
              Text(
                "User Interface: ",
                style: TextStyle(
                  color: Color.fromARGB(255, 200, 173, 246),
                ),
              ),
              Text(
                "Price: ",
                style: TextStyle(
                  color: Color.fromARGB(255, 200, 173, 246),
                ),
              ),
              Text(
                "Value for Money : ",
                style: TextStyle(
                  color: Color.fromARGB(255, 200, 173, 246),
                ),
              ),
              Text(
                "Network : ",
                style: TextStyle(
                  color: Color.fromARGB(255, 200, 173, 246),
                ),
              ),
              Text(
                "Hardware : ",
                style: TextStyle(
                  color: Color.fromARGB(255, 200, 173, 246),
                ),
              ),
              Text(
                "Design : ",
                style: TextStyle(
                  color: Color.fromARGB(255, 200, 173, 246),
                ),
              ),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text("${posts[index].category}"),
              Text("${posts[index].compName}"),
              Text("${posts[index].modelName}"),
              Text("${posts[index].battery}"),
              Text("${posts[index].camera}"),
              Text("${posts[index].performance}"),
              Text("${posts[index].gaming}"),
              Text("${posts[index].charging}"),
              Text("${posts[index].display}"),
              Text("${posts[index].security}"),
              Text("${posts[index].ui}"),
              Text("${posts[index].price}"),
              Text("${posts[index].valueformoney}"),
              Text("${posts[index].network}"),
              Text("${posts[index].hardware}"),
              Text("${posts[index].design}"),
            ],
          ),
        ],
      ),
    ],
  );
}

SingleChildScrollView recommendPosts(
    String category, int index, BuildContext context) {
  if (category == "Mobiles") {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: [
          const SizedBox(
            width: 12,
          ),
          Column(
            children: [
              Container(
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                ),
                child: Image.asset(
                  "assets/mobile_iphone15pro.png",
                  height: 150,
                  width: 120,
                  fit: BoxFit.fill,
                ),
              ),
              const SizedBox(
                height: 8,
              ),
              ElevatedButton(
                onPressed: () {
                  showModalBottomSheet(
                    backgroundColor: Colors.black.withOpacity(0.8),
                    context: context,
                    builder: (context) {
                      return const Column(
                        children: [
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text(
                                    "Category : Mobiles",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 200, 173, 246),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    "Company Name : Apple",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 200, 173, 246),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    "Model Name : Iphone 15 pro",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 200, 173, 246),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      );
                    },
                  );
                },
                style: const ButtonStyle(
                    fixedSize: MaterialStatePropertyAll(Size(85, 30)),
                    backgroundColor: MaterialStatePropertyAll(
                      Color.fromARGB(255, 200, 173, 246),
                    ),
                    foregroundColor: MaterialStatePropertyAll(Colors.black)),
                child: const Text("More"),
              ),
            ],
          ),
          const SizedBox(
            width: 10,
          ),
          Column(
            children: [
              Container(
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                ),
                child: Image.asset(
                  "assets/mobile_pixel_4a.jpg",
                  height: 150,
                  width: 120,
                  fit: BoxFit.fill,
                ),
              ),
              const SizedBox(
                height: 8,
              ),
              ElevatedButton(
                onPressed: () {
                  showModalBottomSheet(
                    backgroundColor: Colors.black.withOpacity(0.8),
                    context: context,
                    builder: (context) {
                      return const Column(
                        children: [
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text(
                                    "Category : Mobiles",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 200, 173, 246),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    "Company Name : Google",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 200, 173, 246),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    "Model Name : Pixel 4",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 200, 173, 246),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      );
                    },
                  );
                },
                style: const ButtonStyle(
                    fixedSize: MaterialStatePropertyAll(Size(85, 30)),
                    backgroundColor: MaterialStatePropertyAll(
                      Color.fromARGB(255, 200, 173, 246),
                    ),
                    foregroundColor: MaterialStatePropertyAll(Colors.black)),
                child: const Text("More"),
              ),
            ],
          ),
          const SizedBox(
            width: 10,
          ),
          Column(
            children: [
              Container(
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                ),
                child: Image.asset(
                  "assets/mobiles_samsungS23.jpg",
                  height: 150,
                  width: 120,
                  fit: BoxFit.fill,
                ),
              ),
              const SizedBox(
                height: 8,
              ),
              ElevatedButton(
                onPressed: () {
                  showModalBottomSheet(
                    backgroundColor: Colors.black.withOpacity(0.8),
                    context: context,
                    builder: (context) {
                      return const Column(
                        children: [
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text(
                                    "Category : Mobiles",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 200, 173, 246),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    "Company Name : Samsung",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 200, 173, 246),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    "Model Name : Z Fold 4",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 200, 173, 246),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      );
                    },
                  );
                },
                style: const ButtonStyle(
                    fixedSize: MaterialStatePropertyAll(Size(85, 30)),
                    backgroundColor: MaterialStatePropertyAll(
                      Color.fromARGB(255, 200, 173, 246),
                    ),
                    foregroundColor: MaterialStatePropertyAll(Colors.black)),
                child: const Text("More"),
              ),
            ],
          ),
          const SizedBox(
            width: 10,
          ),
          Column(
            children: [
              Container(
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                ),
                child: Image.asset(
                  "assets/mobiles_nothing_phone1.jpg",
                  height: 150,
                  width: 120,
                  fit: BoxFit.fill,
                ),
              ),
              const SizedBox(
                height: 8,
              ),
              ElevatedButton(
                onPressed: () {
                  showModalBottomSheet(
                    backgroundColor: Colors.black.withOpacity(0.8),
                    context: context,
                    builder: (context) {
                      return const Column(
                        children: [
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text(
                                    "Category : Mobiles",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 200, 173, 246),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    "Company Name : Nothing",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 200, 173, 246),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    "Model Name : phone 1",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 200, 173, 246),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      );
                    },
                  );
                },
                style: const ButtonStyle(
                    fixedSize: MaterialStatePropertyAll(Size(85, 30)),
                    backgroundColor: MaterialStatePropertyAll(
                      Color.fromARGB(255, 200, 173, 246),
                    ),
                    foregroundColor: MaterialStatePropertyAll(Colors.black)),
                child: const Text("More"),
              ),
            ],
          ),
          const SizedBox(
            width: 10,
          ),
          Column(
            children: [
              Container(
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                ),
                child: Image.asset(
                  "assets/mobiles_iphoneX.jpeg",
                  height: 150,
                  width: 120,
                  fit: BoxFit.fill,
                ),
              ),
              const SizedBox(
                height: 8,
              ),
              ElevatedButton(
                onPressed: () {
                  showModalBottomSheet(
                    backgroundColor: Colors.black.withOpacity(0.8),
                    context: context,
                    builder: (context) {
                      return const Column(
                        children: [
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text(
                                    "Category : Mobiles",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 200, 173, 246),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    "Company Name : Apple",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 200, 173, 246),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    "Model Name : Iphone XR",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 200, 173, 246),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      );
                    },
                  );
                },
                style: const ButtonStyle(
                    fixedSize: MaterialStatePropertyAll(Size(85, 30)),
                    backgroundColor: MaterialStatePropertyAll(
                      Color.fromARGB(255, 200, 173, 246),
                    ),
                    foregroundColor: MaterialStatePropertyAll(Colors.black)),
                child: const Text("More"),
              ),
            ],
          ),
          const SizedBox(
            width: 10,
          ),
          Column(
            children: [
              Container(
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                ),
                child: Image.asset(
                  "assets/mobile_pixel_8pro.jpg",
                  height: 150,
                  width: 120,
                  fit: BoxFit.fill,
                ),
              ),
              const SizedBox(
                height: 8,
              ),
              ElevatedButton(
                onPressed: () {
                  showModalBottomSheet(
                    backgroundColor: Colors.black.withOpacity(0.8),
                    context: context,
                    builder: (context) {
                      return const Column(
                        children: [
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text(
                                    "Category : Mobiles",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 200, 173, 246),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    "Company Name : Google",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 200, 173, 246),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    "Model Name : Pixel 8 Pro",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 200, 173, 246),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      );
                    },
                  );
                },
                style: const ButtonStyle(
                    fixedSize: MaterialStatePropertyAll(Size(85, 30)),
                    backgroundColor: MaterialStatePropertyAll(
                      Color.fromARGB(255, 200, 173, 246),
                    ),
                    foregroundColor: MaterialStatePropertyAll(Colors.black)),
                child: const Text("More"),
              ),
            ],
          ),
        ],
      ),
    );
  } else {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: [
          Column(
            children: [
              Container(
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                ),
                child: Image.asset(
                  "assets/laptops_asus.jpeg",
                  height: 150,
                  width: 120,
                  fit: BoxFit.fill,
                ),
              ),
              const SizedBox(
                height: 8,
              ),
              ElevatedButton(
                onPressed: () {
                  showModalBottomSheet(
                    backgroundColor: Colors.black.withOpacity(0.8),
                    context: context,
                    builder: (context) {
                      return const Column(
                        children: [
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text(
                                    "Category : Laptops",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 200, 173, 246),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    "Company Name : Asus",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 200, 173, 246),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    "Model Name : Zenbook S",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 200, 173, 246),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      );
                    },
                  );
                },
                style: const ButtonStyle(
                    fixedSize: MaterialStatePropertyAll(Size(85, 30)),
                    backgroundColor: MaterialStatePropertyAll(
                      Color.fromARGB(255, 200, 173, 246),
                    ),
                    foregroundColor: MaterialStatePropertyAll(Colors.black)),
                child: const Text("More"),
              ),
            ],
          ),
          const SizedBox(
            width: 10,
          ),
          Column(
            children: [
              Container(
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                ),
                child: Image.asset(
                  "assets/laptops_dell.png",
                  height: 150,
                  width: 120,
                  fit: BoxFit.fill,
                ),
              ),
              const SizedBox(
                height: 8,
              ),
              ElevatedButton(
                onPressed: () {
                  showModalBottomSheet(
                    backgroundColor: Colors.black.withOpacity(0.8),
                    context: context,
                    builder: (context) {
                      return const Column(
                        children: [
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text(
                                    "Category : Laptops",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 200, 173, 246),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    "Company Name : Dell",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 200, 173, 246),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    "Model Name : Inspiron 10",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 200, 173, 246),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      );
                    },
                  );
                },
                style: const ButtonStyle(
                    fixedSize: MaterialStatePropertyAll(Size(85, 30)),
                    backgroundColor: MaterialStatePropertyAll(
                      Color.fromARGB(255, 200, 173, 246),
                    ),
                    foregroundColor: MaterialStatePropertyAll(Colors.black)),
                child: const Text("More"),
              ),
            ],
          ),
          const SizedBox(
            width: 10,
          ),
          Column(
            children: [
              Container(
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                ),
                child: Image.asset(
                  "assets/laptops_msi.png",
                  height: 150,
                  width: 120,
                  fit: BoxFit.fill,
                ),
              ),
              const SizedBox(
                height: 8,
              ),
              ElevatedButton(
                onPressed: () {
                  showModalBottomSheet(
                    backgroundColor: Colors.black.withOpacity(0.8),
                    context: context,
                    builder: (context) {
                      return const Column(
                        children: [
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text(
                                    "Category : Laptops",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 200, 173, 246),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    "Company Name : MSI",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 200, 173, 246),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    "Model Name : GF65 thin 10SDR",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 200, 173, 246),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      );
                    },
                  );
                },
                style: const ButtonStyle(
                    fixedSize: MaterialStatePropertyAll(Size(85, 30)),
                    backgroundColor: MaterialStatePropertyAll(
                      Color.fromARGB(255, 200, 173, 246),
                    ),
                    foregroundColor: MaterialStatePropertyAll(Colors.black)),
                child: const Text("More"),
              ),
            ],
          ),
          const SizedBox(
            width: 10,
          ),
          Column(
            children: [
              Container(
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                ),
                child: Image.asset(
                  "assets/lpatops_macbook.jpeg",
                  height: 150,
                  width: 120,
                  fit: BoxFit.fill,
                ),
              ),
              const SizedBox(
                height: 8,
              ),
              ElevatedButton(
                onPressed: () {
                  showModalBottomSheet(
                    backgroundColor: Colors.black.withOpacity(0.8),
                    context: context,
                    builder: (context) {
                      return const Column(
                        children: [
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text(
                                    "Category : Laptops",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 200, 173, 246),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    "Company Name : Apple",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 200, 173, 246),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    "Model Name : Macbook Air",
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 200, 173, 246),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      );
                    },
                  );
                },
                style: const ButtonStyle(
                    fixedSize: MaterialStatePropertyAll(Size(85, 30)),
                    backgroundColor: MaterialStatePropertyAll(
                      Color.fromARGB(255, 200, 173, 246),
                    ),
                    foregroundColor: MaterialStatePropertyAll(Colors.black)),
                child: const Text("More"),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
