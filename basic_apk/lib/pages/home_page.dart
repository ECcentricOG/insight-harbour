import 'package:basic_apk/converter.dart';
import 'package:flutter/material.dart';
import 'posts.dart';
import 'package:basic_apk/functions.dart';
import 'package:basic_apk/reusable_widget/reusable_widget.dart';
import 'package:basic_apk/review_controller.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

const List<String> list = <String>['Laptops', 'Mobiles', 'Bikes', 'Cars'];

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int likeTracker = 0;
  String dropdownValue = list.first;
  String selectedCategory = "";
  XFile? file;
  ImagePicker picker = ImagePicker();
//
//
//
  @override
  void initState() {
    super.initState();
    addToPosts();
  }

//
//
//
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Insight Harbour",
          style: TextStyle(
            fontSize: 20,
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.w500,
            color: Color.fromARGB(255, 200, 173, 246),
            wordSpacing: 5,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.black,
        actions: [
          Container(
            margin: const EdgeInsets.only(
              top: 13,
              left: 2,
              right: 7,
              bottom: 7,
            ),
            child: const Icon(
              Icons.notifications,
              color: Color.fromARGB(255, 200, 173, 246),
            ),
          ),
        ],
      ),
      body: Container(
        color: Colors.black,
        height: MediaQuery.of(context).size.height,
        child: ListView.builder(
          itemCount: posts.length,
          itemBuilder: (context, index) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const SizedBox(
                  height: 20,
                ),
                Center(
                  child: file == null
                      ? const Text("Image is Loading .....")
                      : Image.file(
                          File(posts[index].imgUrl),
                          height: MediaQuery.of(context).size.height * 0.6,
                          width: MediaQuery.of(context).size.width * 0.95,
                          fit: BoxFit.cover,
                        ),
                ),
                const SizedBox(
                  height: 5,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      margin: const EdgeInsets.only(left: 3),
                      child: IconButton(
                        tooltip: "Like",
                        onPressed: () {
                          likeTracker++;
                          posts[index].isLiked = !posts[index].isLiked;
                          setState(() {
                            if (likeTracker % 2 == 1) {
                              posts[index].likeCount++;
                            } else {
                              posts[index].likeCount--;
                            }
                          });
                        },
                        icon: Icon(
                          posts[index].isLiked
                              ? Icons.favorite_rounded
                              : Icons.favorite_outline_outlined,
                          color: posts[index].isLiked
                              ? Colors.red
                              : const Color.fromARGB(255, 200, 173, 246),
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(right: 1),
                      child: IconButton(
                        color: const Color.fromARGB(255, 200, 173, 246),
                        tooltip: "Info",
                        onPressed: () {
                          showModalBottomSheet(
                            context: context,
                            backgroundColor: Colors.black.withOpacity(0.8),
                            builder: (context) {
                              return getInfo(index);
                            },
                          );
                        },
                        icon: const Icon(Icons.info_outline_rounded),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(right: 1),
                      child: IconButton(
                        color: const Color.fromARGB(255, 200, 173, 246),
                        tooltip: "Share",
                        onPressed: () {},
                        icon: const Icon(Icons.share),
                      ),
                    ),
                  ],
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(
                      width: 12,
                    ),
                    Text(
                      likeCountDisplay(posts[index].likeCount),
                      style: const TextStyle(
                        fontSize: 15,
                        color: Color.fromARGB(255, 200, 173, 246),
                      ),
                    ),
                  ],
                ),
                const Row(
                  children: [
                    SizedBox(
                      width: 12,
                    ),
                    Text(
                      "Other Products",
                      style: TextStyle(
                        fontSize: 20,
                        color: Color.fromARGB(255, 200, 173, 246),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 15,
                ),
                recommendPosts(posts[index].category, index, context),
              ],
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.transparent.withOpacity(0.5),
        onPressed: () {
          showModalBottomSheet(
            backgroundColor: Colors.black.withOpacity(0.8),
            context: context,
            builder: (context) {
              return SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Column(
                  children: [
                    const SizedBox(
                      height: 25,
                    ),
                    DropdownMenu(
                      initialSelection: list.first,
                      onSelected: (String? value) {
                        setState(() {
                          dropdownValue = value!;
                          selectedCategory = value;
                        });
                      },
                      dropdownMenuEntries:
                          list.map<DropdownMenuEntry<String>>((String value) {
                        return DropdownMenuEntry<String>(
                            value: value, label: value);
                      }).toList(),
                    ),
                    const SizedBox(
                      height: 25,
                    ),
                    const Text("Choose a file"),
                    const SizedBox(
                      height: 10,
                    ),
                    Container(
                      width: 300,
                      height: 50,
                      margin: const EdgeInsets.fromLTRB(0, 10, 0, 20),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(90),
                      ),
                      child: ElevatedButton(
                        onPressed: () async {
                          final XFile? photo = await picker.pickImage(
                              source: ImageSource.gallery);
                          setState(() {
                            file = photo;
                          });
                        },
                        style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.resolveWith((states) {
                            if (states.contains(MaterialState.pressed)) {
                              return Colors.black;
                            }
                            return const Color.fromARGB(255, 200, 173, 246);
                          }),
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(30))),
                        ),
                        child: const Text(
                          "Upload",
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 25,
                    ),
                    formTextField("Company Name", compName),
                    const SizedBox(
                      height: 25,
                    ),
                    formTextField("Model Name", modelName),
                    const SizedBox(
                      height: 25,
                    ),
                    formTextField("Camera", camera),
                    const SizedBox(
                      height: 25,
                    ),
                    formTextField("Battery", battery),
                    const SizedBox(
                      height: 25,
                    ),
                    formTextField("Performance", performance),
                    const SizedBox(
                      height: 25,
                    ),
                    formTextField("Gaming", gaming),
                    const SizedBox(
                      height: 25,
                    ),
                    formTextField("Charging", charging),
                    const SizedBox(
                      height: 25,
                    ),
                    formTextField("Security", security),
                    const SizedBox(
                      height: 25,
                    ),
                    formTextField("Display", display),
                    const SizedBox(
                      height: 25,
                    ),
                    formTextField("User Interface", ui),
                    const SizedBox(
                      height: 25,
                    ),
                    formTextField("Price", price),
                    const SizedBox(
                      height: 25,
                    ),
                    formTextField("Value for money", valueformoney),
                    const SizedBox(
                      height: 25,
                    ),
                    formTextField("Network", network),
                    const SizedBox(
                      height: 25,
                    ),
                    formTextField("Hardware", hardware),
                    const SizedBox(
                      height: 25,
                    ),
                    formTextField("Design", design),
                    const SizedBox(
                      height: 25,
                    ),
                    Container(
                      width: 300,
                      height: 50,
                      margin: const EdgeInsets.fromLTRB(0, 10, 0, 20),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(90),
                      ),
                      child: ElevatedButton(
                        onPressed: () async {
                          Post p = Post(
                            category: selectedCategory,
                            modelName: modelName.text,
                            compName: compName.text,
                            imgUrl: convertToFile(file!).path,
                            battery: battery.text,
                            camera: camera.text,
                            charging: charging.text,
                            design: design.text,
                            display: display.text,
                            gaming: gaming.text,
                            hardware: hardware.text,
                            network: network.text,
                            performance: performance.text,
                            price: price.text,
                            security: security.text,
                            ui: ui.text,
                            valueformoney: valueformoney.text,
                          );
                          insertPost(p);
                          await addToPosts();
                          setState(() {});
                          modelName.clear();
                          compName.clear();
                          camera.clear();
                          battery.clear();
                          performance.clear();
                          gaming.clear();
                          charging.clear();
                          display.clear();
                          security.clear();
                          ui.clear();
                          price.clear();
                          valueformoney.clear();
                          network.clear();
                          hardware.clear();
                          design.clear();
                          Navigator.pop(context);
                        },
                        style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.resolveWith((states) {
                            if (states.contains(MaterialState.pressed)) {
                              return Colors.black;
                            }
                            return const Color.fromARGB(255, 200, 173, 246);
                          }),
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(30))),
                        ),
                        child: const Text(
                          "Submit",
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              );
            },
          );
        },
        child: const Icon(
          Icons.add,
          color: Color.fromARGB(255, 200, 173, 246),
        ),
      ),
    );
  }
}
