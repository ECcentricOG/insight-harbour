import 'dart:io';
import 'package:image_picker/image_picker.dart';

File convertToFile(XFile xFile) {
  String path = xFile.path;
  File file = File(path);
  return file;
}
