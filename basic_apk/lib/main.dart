import 'package:basic_apk/pages/posts.dart';
import 'package:flutter/material.dart';
// ignore: depend_on_referenced_packages
import 'package:path/path.dart';
// ignore: depend_on_referenced_packages
import 'package:sqflite/sqflite.dart';
import 'package:basic_apk/pages/signin.dart';

dynamic database;

/*Future<void> */ main() async {
  //WidgetsFlutterBinding.ensureInitialized();
  /*await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );*/
  runApp(const MainApp());
  database = openDatabase(
    join(await getDatabasesPath(), "AppDB.db"),
    version: 1,
    onCreate: (db, version) async {
      await db.execute('''
          CREATE TABLE Post(
            category TEXT,
            imgUrl BLOB,
            compName TEXT,
            modelName TEXT,
            battery TEXT,
            camera TEXT,
            performance TEXT,
            gaming TEXT,
            charging TEXT,
            display TEXT,
            security TEXT,
            ui TEXT,
            price TEXT,
            valueformoney TEXT,
            network TEXT,
            hardware TEXT,
            design TEXT
          )
        ''');
    },
  );
  await addToPosts();
  print(posts.length);
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: const SignInScreen(),
      theme: ThemeData.dark(),
      debugShowCheckedModeBanner: false,
      color: Colors.black,
    );
  }
}
